package com.myhouse.auhorizationService.myhouseauthorizationservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyhouseAuthorizationServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyhouseAuthorizationServiceApplication.class, args);
	}

}
