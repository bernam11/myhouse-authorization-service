package com.myhouse.auhorizationService.myhouseauthorizationservice.repository;

import com.myhouse.auhorizationService.myhouseauthorizationservice.model.Authorization;
import org.springframework.data.cassandra.repository.CassandraRepository;

public interface AuthorizationRepository extends CassandraRepository<Authorization,String> {

}
