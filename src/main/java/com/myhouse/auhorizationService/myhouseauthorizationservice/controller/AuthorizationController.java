package com.myhouse.auhorizationService.myhouseauthorizationservice.controller;

import com.myhouse.auhorizationService.myhouseauthorizationservice.model.Authorization;
import com.myhouse.auhorizationService.myhouseauthorizationservice.repository.AuthorizationRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import com.myhouse.auhorizationService.myhouseauthorizationservice.ResouceNotFoundException;

import javax.websocket.server.PathParam;
import java.util.List;

@RestController
@RequestMapping("/api")
public class AuthorizationController {

    @Autowired
    AuthorizationRepository authorizationRepository;

    @PostMapping("/authorizations")
    public Authorization addAuthorization(@RequestBody Authorization authorization){
        authorizationRepository.save(authorization);
        return authorization;
    }

    @GetMapping("/authorizations/{id}")
    public ResponseEntity<Authorization> findById(@PathParam("id")String id){
        Authorization authorization = authorizationRepository.findById(id)
                .orElseThrow(() -> new ResouceNotFoundException("Authorization not found for this id :: "+id));
        return ResponseEntity.ok().body(authorization);
    }

    @GetMapping("/authorizations")
    public List<Authorization> getAuthorizations(){
        return authorizationRepository.findAll();
    }

    @PutMapping("authorizations/{id}")
    public ResponseEntity<Authorization> updateAuthorization(@PathVariable(value = "id") String id,
                                                             @RequestBody Authorization authorizationDetails){
        Authorization authorization = authorizationRepository.findById(id)
                .orElseThrow(()->new ResouceNotFoundException("Authorization not found for this id :: "+id));

        String[] keys = id.split("_");
        authorization.setAuthorizationLevel(authorizationDetails.getAuthorizationLevel());
        authorization.setUserId(keys[0]+keys[1]);
        authorization.setServiceId(keys[2]);
        final Authorization updatedAuthorization = authorizationRepository.save(authorization);
        return ResponseEntity.ok(updatedAuthorization);
    }
    @DeleteMapping("products/{id}")
    public ResponseEntity<Void> deleteAuthorization(@PathVariable(value = "id") String id){
        Authorization authorization = authorizationRepository.findById(id)
                .orElseThrow(() -> new ResouceNotFoundException("Authorization not found for this id :: "+id));
        authorizationRepository.delete(authorization);
        return ResponseEntity.ok().build();
    }
}
