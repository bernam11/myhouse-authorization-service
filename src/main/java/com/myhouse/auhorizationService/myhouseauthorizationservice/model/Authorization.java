package com.myhouse.auhorizationService.myhouseauthorizationservice.model;

import lombok.*;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;


@Table
@Data
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter

public class Authorization{
    @PrimaryKey
    private String id;

    private String userId;

    private String serviceId;

    private String authorizationLevel;
}